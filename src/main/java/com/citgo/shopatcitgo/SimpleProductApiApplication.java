package com.citgo.shopatcitgo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.citgo")
public class SimpleProductApiApplication {
	
	
	public static void main(String[] args) {
		SpringApplication.run(SimpleProductApiApplication.class, args);
	}

}
