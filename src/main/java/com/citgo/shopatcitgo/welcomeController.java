package com.citgo.shopatcitgo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class welcomeController {

	//welcome
	@GetMapping("/welcome")
	public String welcome() {
		return "  Welcome to citgo at Rockmart";
	}

}
