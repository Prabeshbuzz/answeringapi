package com.citgo.shopatcitgo.service;


import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.citgo.shopatcitgo.model.Product;
import com.citgo.shopatcitgo.model.ProductType;

@Component
public class ProductService {
	
	private static List<ProductType> productTypes = new ArrayList<>();
	static {
		Product product1 = new Product("Newport100",
				"Newport", "180", "Tobacco Product");
		Product product2 = new Product("BMregular",
				"Black & Mild", "100", "Cigarettle");
		Product product3 = new Product("103",
				"Frito Lays", "10", "Small frito lays");
		Product product4 = new Product("104",
				"Corona", "45", "Beer");
		
		List<Product> products = new ArrayList<>(Arrays.asList(product1,product2,product3,product4));
		

		 ProductType productType = new ProductType("test", "Groceries","Home made groceries",products);
		 ProductType productType1 = new ProductType("1002", "Beer","Home made Beer",products);

		productTypes.add(productType);
		productTypes.add(productType1);
	}

	public List<ProductType> retrieveAllProductTypes() {
		return productTypes;
	}

	public ProductType retrieveProductType(String productId) {
		for (ProductType productType : productTypes) {
			if (productType.getProductId().equals(productId)) {
				return productType;
			}
		}
		return null;
	}

	public List<Product> retrieveProducts(String productTypeId) {
		ProductType productType = retrieveProductType(productTypeId);

		if (productType == null) {
			return null;
		}

		return productType.getProduct();
	}

	public Product retrieveProduct(String productTypeId, String ProductId) {
		ProductType productType = retrieveProductType(productTypeId);

		if (productType == null) {
			return null;
		}

		for (Product product : productType.getProduct()) {
			if (product.getProductId().equals(ProductId)) {
				return product;
			}
		}

		return null;
	}

	private SecureRandom random = new SecureRandom();

	public Product addProduct(String productTypeId, Product product) {
		ProductType productType = retrieveProductType(productTypeId);

		if (productType == null) {
			return null;
		}

		String randomId = new BigInteger(130, random).toString(32);
		product.setProductId(randomId);

		productType.getProduct().add(product);

		return product;
	}
}
