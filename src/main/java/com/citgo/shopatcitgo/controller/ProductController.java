package com.citgo.shopatcitgo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.citgo.shopatcitgo.model.Product;
import com.citgo.shopatcitgo.model.ProductType;
import com.citgo.shopatcitgo.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@GetMapping("/productTypes/{productId}/products")
	public List<Product> retriveProducts(@PathVariable String productId) {
		
		
		return productService.retrieveProducts(productId);
		
	}
	
	@GetMapping("allproducttypes")
	public List<ProductType> retrieveAllProductTypes() {
		return productService.retrieveAllProductTypes();
	}
	
}
